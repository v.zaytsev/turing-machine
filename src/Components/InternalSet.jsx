import React, { useState } from "react";

const InternalSet = () => {
  const [count, setCount] = useState(1);
  const [status, setStatus] = useState(["{", "Ω", "}"]);
  function increment() {
    if (status.length <= 21) {
      status.pop();
      setStatus(status);
      status.push(String(", " + "q" + count));
      setStatus(status);
      status.push("}");
      setStatus(status);
      setCount(count + 1);
    }
  }
  function decrement() {
    if (count - 1 >= 1) {
      status.pop();
      setStatus(status);
      status.pop();
      setStatus(status);
      status.push("}");
      setStatus(status);
      setCount(count - 1);
    } else {
      setCount(1);
    }
  }
  return (
    <div className="post0">
      <div className="post_content">
        <strong>1. Внешний алфавит</strong>
        <br />
        <h1>{status}</h1>
      </div>
      <div>
        <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Количество состояний = {count}</h1>
        <br />
        <button className="but_int" style={{ marginRight: 35, marginLeft: 20 }} onClick={increment}>
          Увеличить
        </button>
        <button className="but_int" onClick={decrement}>Уменьшить</button>
      </div>
    </div>
  );
};

export default InternalSet;
