import React, { useState } from "react";

const ExternalSet = () => {
  const [char, setChar] = useState(["{", "}"]);
  const [value, setValue] = useState("");
  function addcharacters() {
    if (
      char.length > 2 &&
      value != "" &&
      value.length == 1 &&
      ((value >= "a" && value <= "z") ||
        (value >= "A" && value <= "Z") ||
        (value >= "0" && value <= "9"))
    ) {
      char.pop();
      setChar(char);
      char.push("," + value);
      setChar(char);
      char.push("}");
      setValue("");
      setChar(char);
    } else if (
      value != "" &&
      value.length == 1 &&
      ((value >= "a" && value <= "z") ||
        (value >= "A" && value <= "Z") ||
        (value >= "0" && value <= "9"))
    ) {
      char.pop();
      setChar(char);
      char.push(value);
      setChar(char);
      char.push("}");
      setValue("");
      setChar(char);
    }
  }
  return (
    <div className="post1">
      <div className="post_content">
        <strong>2. Внешний алфавит</strong>
        <br />
        <h1>{char}</h1>
      </div>
      <div>
        <h1 style={{ paddingBottom: 10 }}>&nbsp;Введите элемент</h1>
        <input
          type="text"
          style={{ width: 150, marginRight: 50 }}
          onChange={(event) => setValue(event.target.value)}
          value={value}
        />
        <button className="but_ext" onClick={addcharacters}>Добавить</button>
      </div>
    </div>
  );
};

export default ExternalSet;
