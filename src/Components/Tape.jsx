import React, { useState } from "react";
import Cell from "./Cell";

const Tape = () => {
  const [value, setValue] = useState("");
  const [tape, setTape] = useState([]);
  const [cells, setCells] = useState([
    { id: 1, value: 0},
    { id: 2, value: 0},
    { id: 3, value: 0},
    { id: 4, value: 0},
    { id: 5, value: 0},
    { id: 6, value: 0},
    { id: 7, value: 0},
    { id: 8, value: 0},
    { id: 9, value: 0},
    { id: 10, value: 0},
    { id: 11, value: 0},
    { id: 12, value: 0},
    { id: 13, value: 0},
    { id: 14, value: 0},
  ]);
  const [right, setRight] = useState(cells.length - 1);
  const [left, setLeft] = useState(0);
  const [currNow, setCurrNow] = useState(0);
  const [currAll, setCurrAll] = useState(0);
  function addTape() {
    let chr = value.split("");
    for (let i = 0; i < chr.length; ++i) {
      if (!(
        chr[i] != " " &&
        (chr[i] >= "a" && chr[i] <= "z") ||
        (chr[i] >="A" && chr[i] <= "Z") ||
        (chr[i] >= "0" && chr[i] <= "9")
      )) {
        alert("Неккоректный ввод.");
        return;
      }
    }
    if ((chr.length <= cells.length - currNow) & (cells.length <= 14)) {
      for (let i = 0; i < chr.length; ++i) {
        cells[currNow + i].value = chr[i];
      }
      for (let i = 0; i < chr.length; ++i) {
        tape.push(chr[i]);
      }
      setCurrNow(currNow + chr.length);
      setCurrAll(currAll + chr.length);
      setCells(cells);
      setTape(tape);
    } else {
      let diff = -(cells.length - currNow) + chr.length;
      for (let i = 0; i < chr.length; ++i) {
        tape.push(chr[i]);
      }
      //alert(left);
      setTape(tape);
      for (let i = 0; i < 14; ++i) {
        cells[i].value = tape[left + diff + i];
      }
      setLeft(left + diff);
      setRight(right + diff);
      setCurrAll(currAll + diff);
      setCurrNow(14);
    }
  }

  function forwardStep() {
    tape.push(0);
    for (let i = 0; i < 14; ++i) {
      cells[i].value = tape[left + 1 + i];
    }
    setLeft(left + 1);
    setRight(right + 1);
    setCurrAll(currAll + 1);
    setCurrNow(14);
  }

  function clearTape() {
    for (let i = 0; i < 14; ++i) {
      cells[i].value = 0;
    }
    setCells(cells);
    setTape([]);
    setCurrNow(0);
    setCurrAll(0);
    setLeft(0);
    setRight(cells.length - 1);
  }
  return (
    <div className="post2">
      <div className="post_content">
        <strong>3. Лента</strong>
        <div class="container">
          <div class="box">
            {cells.map((cell) => (
              <Cell cell={cell} key={cell.id} />
            ))}
          </div>
        </div>
      </div>
      <div>
        <h1 style={{ paddingBottom: 10 }}>&nbsp;&nbsp;&nbsp;Введите слово</h1>
        <input
          type="text"
          style={{ width: 150, marginRight: 50 }}
          onChange={(event) => setValue(event.target.value)}
          value={value}
        />
        <button className="but0_tape" onClick={addTape}>
          Поместить
        </button>
        <br />
        <br />
        <button className="but1_tape" onClick={forwardStep}>
          Сделать шаг
        </button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button className="but2_tape" onClick={clearTape}>
          Очистить ленту
        </button>
      </div>
    </div>
  );
};

export default Tape;
