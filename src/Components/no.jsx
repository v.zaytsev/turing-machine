import React, { useState } from "react";
import { __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED } from "react";

const Tape = () => {
  const [value, setValue] = useState("");
  const [tape, setTape] = useState([]);
  const [curr, setCurr] = useState(0);
  const [right, setRight] = useState(13);
  const [left, setLeft] = useState(0);
  function addtape() {
    alert(curr);
    let chr = value.split("");
    if (chr.length <= 13 - curr) {
      let row = document.getElementById("tape");
      let cells = row.getElementsByTagName("td");
      if (tape.length == 0) {
        for (let i = 0; i < chr.length; ++i) {
          cells[i].innerHTML = chr[i];
        }
        setCurr(chr.length - 1);
        for (let i = 0; i < chr.length; ++i) {
          tape.push(chr[i]);
        }
        setTape(tape);
      } else {
        alert("left - " + left);
        for (let i = 0; i < chr.length; ++i) {
          tape.push(chr[i]);
        }
        if (tape.length > 14) {
          setLeft(chr.length + left);
          setRight(chr.length + right);
          setTape(tape);
          for (let i = 0; i < 14; ++i) {
            cells[i].innerHTML = tape[left + i + chr.length];
            //j += 1;
          }
          setCurr(curr + chr.length);
        }
        else{
        setTape(tape);
        for (let i = 0; i < 14; ++i) {
          cells[i].innerHTML = tape[left + i];
          //j += 1;
        }
        setCurr(curr + chr.length);
      }}
    } else {
      let difference = chr.length - (13 - curr);
      for (let i = 0; i < chr.length; ++i) {
        tape.push(chr[i]);
      }
      setTape(tape);
      let row = document.getElementById("tape");
      let cells = row.getElementsByTagName("td");
      //let j = (curr + difference - 1) % 13;
      alert("left - " + left);
      for (let i = 0; i < 14; ++i) {
        cells[i].innerHTML = tape[difference + i + left];
      }
      setLeft(left + difference);
      setRight(right + difference);
      setCurr((curr + difference - 1) % 13);
    }
  }
  return (
    <div className="post2">
      <div className="post_content">
        <strong>3. Лента</strong>
        <table className="tab0" border="0" style={{ marginTop: 10 }}>
          <tr id="tape">
            <td id="cell0">0</td>&nbsp;
            <td id="cell1">0</td>&nbsp;
            <td id="cell2">0</td>&nbsp;
            <td id="cell3">0</td>&nbsp;
            <td id="cell4">0</td>&nbsp;
            <td id="cell5">0</td>&nbsp;
            <td id="cell6">0</td>&nbsp;
            <td id="cell7">0</td>&nbsp;
            <td id="cell8">0</td>&nbsp;
            <td id="cell9">0</td>&nbsp;
            <td id="cell10">0</td>&nbsp;
            <td id="cell11">0</td>&nbsp;
            <td id="cell12">0</td>&nbsp;
            <td id="cell13">0</td>&nbsp;
          </tr>
        </table>
      </div>
      <div>
        <h1 style={{ paddingBottom: 10 }}>&nbsp;&nbsp;&nbsp;Введите слово</h1>
        <input
          type="text"
          style={{ width: 150, marginRight: 50 }}
          onChange={(event) => setValue(event.target.value)}
          value={value}
        />
        <button className="but0_tape" onClick={addtape}>
          Поместить
        </button>
        <br />
        <br />
        <button className="but1_tape" onClick={addtape}>
          Сделать шаг
        </button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button className="but2_tape" onClick={addtape}>
          Очистить ленту
        </button>
      </div>
    </div>
  );
};

export default Tape;
