import React, { useState } from "react";
import ExternalSet from "./Components/ExternalSet";
import InternalSet from "./Components/InternalSet";
import Tape from "./Components/Tape";
import "./Styles/App.css"

function App() {
  return (
    <div className="App">
      <InternalSet/>
      <ExternalSet/>
      <Tape/>
    </div>
  );
}

export default App;
